@extends('layouts.app')

@section('content')
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Category</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Birthday Date</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($clients as $client)
            <tr>
                <td>{{ $client->category->title }}</td>
                <td>{{ $client->first_name }}</td>
                <td>{{ $client->last_name }}</td>
                <td>{{ $client->email }}</td>
                <td>{{ $client->gender }}</td>
                <td>{{ $client->birthday_date }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <div class="d-flex justify-content-center">
        {!! $clients->links('vendor.pagination.bootstrap-4') !!}
    </div>

@endsection
