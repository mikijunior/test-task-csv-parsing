# Laravel test task

## Steps to run a project
1. Clone the repository
2. Fill the .env file by example on .env.example
3. Run docker containers: `make run`
4. Start migrations: `make migrate`
5. Start parsing the CSV file via the command: `make parse csv`
6. Open http://localhost:8000 there you can see a simple paginated page with a list of parsed clients. 
Also, you can use API endpoint `http://localhost:8000/api/clients` to get access to data  
