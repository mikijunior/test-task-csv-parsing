run:
	sudo docker-compose up -d

down:
	sudo docker-compose down

migrate:
	sudo docker exec -it laravel-docker-app php artisan migrate

parse-csv:
	sudo docker exec -it laravel-docker-app php artisan csv:parse
