<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Client extends Model
{
    public const GENDERS = [
        'male',
        'female',
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'gender',
        'birthday_date',
        'category_id',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

}
