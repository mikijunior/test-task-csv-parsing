<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Client;
use App\Services\CSVReader;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class CsvParse extends Command
{
    protected $signature = 'csv:parse';

    protected $description = 'Parse provided CSV';

    protected CSVReader $reader;

    public function __construct(CSVReader $reader)
    {
        $this->reader = $reader;
        parent::__construct();
    }

    public function handle()
    {
        $csvFilePath = public_path('dataset.csv');
        $csvData = $this->reader->getSCVContent($csvFilePath);

        $bar = $this->output->createProgressBar(count($csvData));

        $bar->start();
        foreach ($csvData as $row) {
            $this->storeParsedRow($row);
            $bar->advance();
        }

        $bar->finish();
        $this->output->success('File successfully parsed');
    }

    private function storeParsedRow($row)
    {
        DB::transaction(function () use ($row) {
            $category = Category::firstOrCreate([
                'title' => $row['category_title'],
            ]);

            $row['category_id'] = $category->id;

            /** @var Client $client */
            Client::firstOrCreate(Arr::except($row, 'category_title'));
        });
    }
}
