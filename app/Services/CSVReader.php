<?php

namespace App\Services;
use SplFileObject;

class CSVReader
{
    public function getSCVContent(string $filePath): array
    {
        $file = new SplFileObject($filePath, 'r');
        $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY);

        $headers = null;
        $data = [];

        foreach ($file as $row) {
            if (empty($row)) {
                continue;
            }

            if (!$headers) {
                $headers = $this->mapHeaders($row);
                continue;
            }

            $data[] = array_combine($headers, $row);
        }

        unset($file);

        return $data;
    }

    private function mapHeaders(array $headers): array
    {
        $mapping = [
            'category' => 'category_title',
            'firstname' => 'first_name',
            'lastname' => 'last_name',
            'email' => 'email',
            'gender' => 'gender',
            'birthDate' => 'birthday_date',
        ];

        return array_map(fn($item) => $mapping[$item], $headers);
    }
}
